import React, { Component } from 'react';
import { Route } from 'react-router-dom'

import Home from '../pages/Home';
import ManageGroups from '../pages/ManageGroups';
import MyProfile from '../pages/MyProfile';
import logo from '../images/logo.png'
import DashboardButton from '../components/DashboardButton';
import { AnimatedSwitch } from 'react-router-transition'

import './Dashboard.css';

export default class Dashboard extends Component {
  redirectTo = (path) => {
    this.props.history.push(`${this.props.match.url}/${path}`);
  }

  render() {
    return (
      <div className="dashboard">
        <div className="dashboard-navbar">
          <img src={logo} alt="logo" className="dashboard-navbar-logo ml-2"/>
          <div onClick={() => this.redirectTo('home')}>
            <DashboardButton text="Home" />
          </div>
          <div onClick={() => this.redirectTo('my-profile')}>
            <DashboardButton text="My Profile" />
          </div>
          <div onClick={() => this.redirectTo('manage-groups')}>
            <DashboardButton text="Manage Groups" />
          </div>
        </div>
        <div className="dashboard-page">
          <AnimatedSwitch
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            className="switch-wrapper"
          >
            <Route path={`${this.props.match.url}/home`} component={Home} />
            <Route path={`${this.props.match.url}/my-profile`} component={MyProfile} />
            <Route path={`${this.props.match.url}/manage-groups`} component={ManageGroups} />
          </AnimatedSwitch>
        </div>
      </div>
    );
  }
}
