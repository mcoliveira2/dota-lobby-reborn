import React, { Component } from 'react';
import { IoIosArrowDropdown, IoIosArrowDropup } from "react-icons/io";

import 'react-table/react-table.css'
import './ManageGroups.css';

import api from '../services/api';


export default class ManageGroups extends Component {
  state = {
    groups: []
  }

  componentDidMount = async _ => {
    api.get(`/group/findGroupsByAdminAccount?admin_account_id=903376573`)
      .then(response => {
        this.setState({ groups: response.data });
      })
      .catch(error => {
        console.log(error.response);
      })
  }


  render() {

    return (
      <div className="manage-groups">
        <div className="row">
          
        </div>
      </div>
    );
  }
}
