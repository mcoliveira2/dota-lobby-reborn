import axios from 'axios'

const api = axios.create({
    baseURL: 'http://172.30.64.53:3002',
    timeout: 15000,
    headers: {
        //'Authorization': localStorage.getItem('@token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'crossDomain': true
    }
})

export default api;