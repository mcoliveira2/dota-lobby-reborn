import React, { Component } from 'react';

 import './DashboardButton.css';

export default class DashboardButton extends Component {
  render() {
    return (
      <div className="dashboard-button-box">
        <a href="#" className="dashboard-button">{this.props.text}</a>
      </div>
    );
  }
}
