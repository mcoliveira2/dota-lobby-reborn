const playerService = require("../services/player.service.js");
const Player = require("../models/player.model.js");
let controller = {};

controller.searchApiPlayers = async function (req, res) {
    try {
        if(req.query && (req.query.personaname || req.query.account_id)) {
            let player = new Player();
            if (req.query.personaname) player.personaname = req.query.personaname;
            if (req.query.account_id) player.account_id = req.query.account_id;
            let players = await playerService.searchApiPlayers(player);
            return res.status(200).json({ status: 200, data: players, message: "Succesfully Searched Players" });
        } else {
            return res.status(200).json({ status: 400, message: "Personaname or Account_id is required" });
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

controller.savePlayer = async function (req, res) {
    try {
        if (req.body && req.body.account_id) {
            await playerService.savePlayer(req.body);
            return res.status(200).json({ status: 200, message: "Succesfully Saved Player" });
        } else {
            return res.status(200).json({ status: 400, message: "Player Account_id is required" });
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

module.exports = controller;
