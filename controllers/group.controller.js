const groupService = require("../services/group.service.js");
const Group = require("../models/group.model.js");
let controller = {};

controller.newGroup = async function (req, res) {
    try {
        if(req.body != null && req.body.groupName != null) {
            let group = new Group(req.body.groupName);
            await groupService.newGroup(group);
            return res.status(200).json({ status: 200, message: "Succesfully Registered Group" });
        } else {
            return res.status(200).json({ status: 400, message: "groupName is required" });
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

controller.addPlayerToGroup = async function (req, res) {
    try {
        if(req.body  && req.body.player && req.body.groupName ) {
            await groupService.addPlayerToGroup(req.body.player, req.body.groupName);
            return res.status(200).json({ status: 200, message: "Succesfully Registered player on group" });
        } else {
            return res.status(200).json({ status: 400, message: "groupName and player is required" });
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

controller.findGroupByName = async function(req, res) {
    try {
        if(req.query && req.query.groupName) {
            let group = await groupService.findGroupByName(req.query.groupName);
            return res.status(200).json(group.docs);
        } else {
            return res.status(200).json({ status: 400, message: "groupName is required" });
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

controller.findGroupsByAccount = async function (req, res) {
    try {
        if(req.query && req.query.admin_account_id) {
            let group = await groupService.findGroupsByAccount(req.query.admin_account_id);
            return res.status(200).json(group.docs);
        } else {
            return res.status(200).json({ status: 400, message: "admin_account_id is required" });
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

module.exports = controller;
