const PouchDB = require('pouchdb');
PouchDB.plugin(require('pouchdb-find'));
const dbConfig = require('../config/database.config.js');
const playerService = require('./player.service.js');
let db = new PouchDB(dbConfig.url);
let service = {};

service.newGroup = async function(newGroup) {
    if (newGroup != null) {
        let response = await this.findGroupByName(newGroup.name);
        newGroup.players = [];
        newGroup.admin_account_id = 903376573;
        if (response.docs && response.docs.length == 0) {
            await db.post({
                "group": newGroup, 
                _id: newGroup.name,
            }).then((response) =>{
                return response;
            }).catch(function (e) {
                throw Error(e)
            });
        } else {
            throw Error('Group already exists')
        }
    }
}

service.findGroupByName = async function(groupName) {
    try{
       return await db.find({
            selector: { "group.name": {$eq: groupName}}
        })
    } catch (e) {
        throw Error('Error while finding group')
    }
}

service.findGroupsByAccount = async function(admin_account_id) {
    try{
        return await db.find({
            selector: { "group.admin_account_id": {$eq: Number(admin_account_id)}}
        })
    } catch (e) {
        throw Error('Error while finding group')
    }
}

service.addPlayerToGroup = async function(player, groupName) {
    if (!player) {
        throw Error('Player is required');
    } 
    if (!groupName) {
        throw Error('Group name is required');
    } 
    
    let groupDocs = await this.findGroupByName(groupName);
    let savedGroup = groupDocs.docs[0] ? groupDocs.docs[0].group : null;
    let isPlayerInGroup;

    savedGroup.players.forEach((playerSaved) => {
        if(playerSaved.account_id == player.account_id) {
            isPlayerInGroup = true;
        }
    });
    
    if(isPlayerInGroup) {
        throw Error('Player is actually in group');
    }

    if (savedGroup) {
        await playerService.savePlayer(player);
        savedGroup.players.push(player);
        db.put({
            _id: groupName,
            _rev: groupDocs.docs[0]._rev,
            group: savedGroup
        });
    } else {
        throw Error('Group does not exists');
    }
}

module.exports = service;