const PouchDB = require('pouchdb');
const fetch = require("node-fetch");
PouchDB.plugin(require('pouchdb-find'));
const dbConfig = require('../config/database.config.js');
const apiConfig = require('../config/api.config.js');
let db = new PouchDB(dbConfig.url);
let service = {};

service.searchApiPlayers = function(filter) {
    let url = apiConfig.EXPLORER+'select%20*%20from%20players%20where%201=1';
    if (filter.personaname) {
        url = url + '%20and%20personaname%20like%20%27%25'+filter.personaname+'%25%27%20';
    }
    if(filter.account_id) {
        url = url + '%20and%20account_id='+filter.account_id;
    }
    try {
        return new Promise( resolve => {
            fetch(url)
                .then( j => j.json())
                .then( players => {
                    resolve(players);
                })
        });
    } catch (e) {
        throw Error(e.message);
    }
}

service.savePlayer = async function(player) {
    if (player && player.account_id) {
        let response = await db.find({
            selector: { "player.account_id": {$eq: Number(player.account_id)}}
        }).catch((e) => {
            throw Error(e.message);
        });
        if (response && response.docs && !response.docs.length) {
            return await db.put({
                "player": player,
                _id: player.account_id.toString()
            });
        } else {
            return await db.put({
                _id: player.account_id.toString(),
                _rev: response.docs[0]._rev,
                player: player
            });
        }
    } else {
        throw Error('Error while creating player')
    }
}

module.exports = service;

