const express = require("express");
const playerController = require("../controllers/player.controller.js");
const router = express.Router();

router
  .route("/")
  .get(playerController.searchApiPlayers)
  .post(playerController.savePlayer)

  module.exports = router;