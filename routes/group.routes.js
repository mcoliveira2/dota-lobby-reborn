const express = require("express");
const groupController = require("../controllers/group.controller.js");
const router = express.Router();

router
  .route("/")
  .post(groupController.newGroup)
  .get(groupController.findGroupByName);

router
  .route("/addPlayerToGroup")
  .post(groupController.addPlayerToGroup);

router
  .route("/findGroupsByAccount")
  .get(groupController.findGroupsByAccount);


  module.exports = router;