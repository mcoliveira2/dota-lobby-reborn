let Group = function (name) {
    this.name = name;
}

Group.prototype.setName = function (name) {
    this.name = name;
}

Group.prototype.getName = function () {
    return this.name;
}

module.exports = Group;