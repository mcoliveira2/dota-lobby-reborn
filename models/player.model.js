let Player = function () {}

Player.prototype.setPersonaname = function (personaname) {
    this.personaname = personaname;
}

Player.prototype.getPersonaname = function () {
    return this.personaname;
}

Player.prototype.setAccountId = function (account_id) {
    this.account_id = account_id;
}

Player.prototype.getAccountId = function () {
    return this.account_id;
}

module.exports = Player;