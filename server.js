const express = require('express');
const PouchDB = require('pouchdb');
const cors = require('cors');
const bodyParser = require('body-parser')
const dbConfig = require('./config/database.config.js');
const groupRouter = require('./routes/group.routes.js');
const playerRouter = require('./routes/player.routes.js');
const app = express();
const PORT = 3002;
const db = new PouchDB(dbConfig.url);
let router = express.Router();

db.info().then(function (info) {
    console.log(info);
});

app.use(cors());
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

router.get('/',function(req,res){
    res.json({'message' : 'Ping Successfull'});
});

app.use('/api',router);
app.use('/group',groupRouter);
app.use('/player',playerRouter);

app.listen(PORT,function(){
    console.log('Server is running at PORT:',PORT);
});
