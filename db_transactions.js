let PouchDB = require('pouchdb');
const fetch = require("node-fetch");
let db = new PouchDB("http://localhost:3000/dota_lobby_reborn");

db.info((err,info) => {
 if(!err){
      console.log(info)
 }
});

const API_KEY = 'D7F92DFED584705889CA1F40E04F78D9';

async function buscaPartida(id_partida) {
    let url = 'https://api.opendota.com/api/matches/'+id_partida+'?api_key='+API_KEY;
    return new Promise( resolve => {
        fetch(url)
            .then( j => j.json())
            .then( partida => {
                partida._id = partida.match_id.toString();
                salvaPartida(partida);
                resolve(partida);
            })
    });
}

async function buscaPartidaPorConta(id_conta) {
    let url = 'https://api.opendota.com/api/players/'+id_conta+'/matches?api_key='+API_KEY;
    return new Promise( resolve => {
        fetch(url)
            .then( j => j.json())
            .then( partidas => {
                let partidasDetalhadas = [];
                partidas.forEach(async partida => {
                    await buscaPartida(partida.match_id).then(function(partidaDetalhada) {
                        partidasDetalhadas.push(partidaDetalhada);
                    })
                });
                resolve(partidasDetalhadas);
            })
    });
}

function salvaPartida(partida) {
    db.put(partida, function(err, response) {
        if (err) {
           return console.log(err);
        } else {
           console.log("Partida salva com sucesso", response);
        }
     });
}   
